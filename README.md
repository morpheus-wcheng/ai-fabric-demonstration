# README #

### Viewing the Jupyter Notebook ###

To view the Jupyter notebook within Bit Bucket, please add the Notebook Viewer from the Bit Bucket Market place.

### ML package structure ###

Folder AustralianRainPredictor contains the files and package structure required to deploy ML models within AI Fabric. 

### Data ###

Data was gathered from Kaggle: https://www.kaggle.com/jsphyg/weather-dataset-rattle-package

### Inspirations ###

Inspirations on data cleaning and approach from https://www.kaggle.com/aninditapani/will-it-rain-tomorrow