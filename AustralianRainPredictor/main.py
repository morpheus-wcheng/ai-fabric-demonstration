import joblib
import json
from dataprocessing import WeatherPreprocessor

class Main(object):

	def __init__(self):
		self.preprocessor = WeatherPreprocessor(joblib.load('./models/austrialia_rain_preprocessor.joblib'), joblib.load('./models/dummy_frame.joblib'))
		self.predictor = joblib.load('./models/austrialia_rain_predictor.joblib')

	def predict(self, json_data):
		data = json.loads(json_data)
		preprocessedData = self.preprocessor.process(data)
		probability = self.predictor.predict_proba(preprocessedData)
		payload = {"No Rain": probability[0][0], "Rain": probability[0][1]}
		return json.dumps(payload)