from sklearn import preprocessing
import joblib
import pandas as pd
import numpy as np
import json

class WeatherPreprocessor:

	def __init__(self, scalingModel, dummyFrame):
		self._scalingModel = scalingModel
		self._dummyFrame = dummyFrame

	def process(self, jsonText):
		df = pd.DataFrame.from_dict(jsonText)
		categorical_columns = ['WindGustDir', 'WindDir3pm', 'WindDir9am']
		df = pd.get_dummies(df, columns = categorical_columns)
		df = df.reindex(columns = self._dummyFrame, fill_value=0)
		df = pd.DataFrame(self._scalingModel.transform(df), index=df.index, columns=df.columns)
		return df
	